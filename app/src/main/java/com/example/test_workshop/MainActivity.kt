package com.example.test_workshop

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider

// Aktivität, die die ToDo-Liste anzeigt
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Inhaltsansicht der Aktivität festlegen
        setContent {
            // Liste von Aufgaben
            val tasks = listOf("Task 1", "Task 2", "Task 3")
            // Die TodoList-Composable-Funktion aufrufen und die Liste von Aufgaben 	übergeben
            TodoList(tasks = tasks)
        }
    }
}

// Composable-Funktion zum Anzeigen einer Liste von To-Do-Aufgaben
@Composable
fun TodoList(tasks: List<String>) {
    // LazyColumn, um eine Liste von Elementen effizient anzuzeigen
    LazyColumn(modifier = Modifier.fillMaxSize()) {
        // items-Funktion zum Iterieren über die Liste von Aufgaben und Anzeigen der TodoItem-Composable-Funktion für jedes Element
        items(items = tasks) { task ->
            TodoItem(task = task) // Aufrufen der TodoItem-Composable-Funktion für jede Aufgabe
        }
    }
}

// Composable-Funktion zum Anzeigen eines einzelnen To-Do-Elements
@Composable
fun TodoItem(task: String) {
    // Textansicht für die Aufgabe
    Text(
        text = "• $task", // Text der Aufgabe
        fontSize = 16.sp, // Schriftgröße
        modifier = Modifier // Modifier für Layout-Eigenschaften
            .padding(16.dp) // Innenabstand
    )
}


// Vorschau für die TodoList-Composable-Funktion
@Preview
@Composable
fun PreviewTodoList() {
    val tasks = listOf("Task 1", "Task 2", "Task 3") // Liste von Aufgaben für die Vorschau
    TodoList(tasks = tasks) // Aufrufen der TodoList-Composable-Funktion für die Vorschau
}

// Vorschau für die TodoItem-Composable-Funktion
@Preview
@Composable
fun PreviewTodoItem(@PreviewParameter(TodoItemProvider::class) task: String) {
    TodoItem(task = task) // Aufrufen der TodoItem-Composable-Funktion für die Vorschau
}

// Anbieter für Vorschauwerte für die TodoItem-Composable-Funktion
class TodoItemProvider : PreviewParameterProvider<String> {
    override val values: Sequence<String>
        get() = sequenceOf("Task 1", "Task 2", "Task 3") // Sequenz von Vorschauwerten für die Aufgaben
}